import { Component } from '@angular/core';

import { LayoutComponent } from "./components/layout.component";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Http trailer';
}
