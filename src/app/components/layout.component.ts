import { Component } from '@angular/core';

import { HttpService } from '../services/httpService.component';

@Component({
    selector: 'layout-component',
    template: `
        <div>
            <button (click)='getJson()'>get Json</button>
            <div *ngFor="let d of data">
                <h4> {{d.title}}</h4>
                <p> {{d.body}}</p>
            </div>
        </div>
    `,
    providers: [HttpService]
})
export class LayoutComponent {
    private data: String = '';

    constructor(private _service: HttpService) { }

    getJson() {
        this._service.callGetJson()
            .map((d: any) => d.json())
            .subscribe(
            (text) => {
                this.data = text;
                console.log(text);
            },
            (err) => {
                console.log(err);
            }
            );
    }

}